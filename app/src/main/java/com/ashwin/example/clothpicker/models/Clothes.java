package com.ashwin.example.clothpicker.models;

import java.util.ArrayList;

/**
 * Created by Ashwin on 24-09-2017.
 */

public class Clothes {

    public Clothes() { }

    private ArrayList<Cloth> clothes;

    public ArrayList<Cloth> getClothes() {
        return clothes;
    }

    public void setClothes(ArrayList<Cloth> clothes) {
        this.clothes = clothes;
    }
}
