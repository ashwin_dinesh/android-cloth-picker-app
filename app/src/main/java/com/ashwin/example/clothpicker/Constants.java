package com.ashwin.example.clothpicker;

/**
 * Created by Ashwin on 22-09-2017.
 */

public class Constants {
    public static final String DEBUG_LOGGING = "debuglogging";
    public static final String TYPE = "type";
    public static final String SHIRTS = "shirts";
    public static final String PANTS = "pants";
}
