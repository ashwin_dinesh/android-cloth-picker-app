package com.ashwin.example.clothpicker.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ashwin.example.clothpicker.R;

import java.util.ArrayList;

/**
 * Created by Ashwin on 25-09-2017.
 */

public class BookmarksRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface BookmarkActionListener {
        void onShareClicked(int position);
        void onRemoveClicked(int position);
    }

    private Context mContext;
    private BookmarkActionListener mBookmarkActionListener;
    private ArrayList<Bitmap> mBookmarkBitmaps;

    public BookmarksRecyclerAdapter(Context context, BookmarksRecyclerAdapter.BookmarkActionListener bookmarkActionListener, ArrayList<Bitmap> bookmarkBitmaps) {
        mContext = context;
        mBookmarkActionListener = bookmarkActionListener;
        mBookmarkBitmaps = bookmarkBitmaps;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_bookmark, parent, false);
        return new BookmarksRecyclerAdapter.BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BookmarksRecyclerAdapter.BookmarkViewHolder bookmarkViewHolder = (BookmarksRecyclerAdapter.BookmarkViewHolder) holder;
        (bookmarkViewHolder).position = position;
        (bookmarkViewHolder).mImageView.setImageBitmap(mBookmarkBitmaps.get(position));
    }

    @Override
    public int getItemCount() {
        return mBookmarkBitmaps.size();
    }

    private class BookmarkViewHolder extends RecyclerView.ViewHolder {
        int position;
        ImageView mImageView, mRemoveImageView, mShareImageView;
        public BookmarkViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView);

            mRemoveImageView = (ImageView) itemView.findViewById(R.id.removeImageView);
            mRemoveImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mBookmarkActionListener != null) {
                        mBookmarkActionListener.onRemoveClicked(position);
                    }
                }
            });

            mShareImageView = (ImageView) itemView.findViewById(R.id.shareImageView);
            mShareImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mBookmarkActionListener != null) {
                        mBookmarkActionListener.onShareClicked(position);
                    }
                }
            });
        }
    }
}
