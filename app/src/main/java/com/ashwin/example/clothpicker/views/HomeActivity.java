package com.ashwin.example.clothpicker.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ashwin.example.clothpicker.Constants;
import com.ashwin.example.clothpicker.R;
import com.ashwin.example.clothpicker.SharedPreferencesManager;
import com.facebook.login.LoginManager;

public class HomeActivity extends AppCompatActivity {

    private SharedPreferencesManager mSharedPrefs;

    private Button mGoToTodaysWearButton, mGoToBookmarksButton, mLogoutButton;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initData();
        initViews();
        if (mSharedPrefs.getShirts().size() > 0 && mSharedPrefs.getPants().size() > 0) {
            // Stay here
        } else {
            goToAddClothes();
        }
    }

    private void initData() {
        mSharedPrefs = new SharedPreferencesManager(this);
    }

    private void initViews() {
        initGoToTodaysWearButton();
        initGoToBookmarksButton();
        initLogoutButton();
    }

    private void initGoToTodaysWearButton() {
        mGoToTodaysWearButton = (Button) findViewById(R.id.goToTodaysWearButton);
        mGoToTodaysWearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSharedPrefs.getShirts().size() > 0 && mSharedPrefs.getPants().size() > 0) {
                    goToTodaysWear();
                } else {
                    goToAddClothes();
                }
            }
        });
    }

    private void initGoToBookmarksButton() {
        mGoToBookmarksButton = (Button) findViewById(R.id.goToBookmarksButton);
        mGoToBookmarksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToBookmarks();
            }
        });
    }

    private void initLogoutButton() {
        mLogoutButton = (Button) findViewById(R.id.logoutButton);
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    private void goToTodaysWear() {
        Intent intent = new Intent(HomeActivity.this, TodaysWearActivity.class);
        startActivity(intent);
    }

    private void goToBookmarks() {
        Intent intent = new Intent(HomeActivity.this, BookmarksActivity.class);
        startActivity(intent);
    }

    private void goToAddClothes() {
        Intent intent = new Intent(HomeActivity.this, AddClothesActivity.class);
        intent.putExtra(Constants.TYPE, Constants.SHIRTS);
        startActivity(intent);
    }

    private void logout() {
        LoginManager.getInstance().logOut();
        SharedPreferencesManager sharedPrefs = new SharedPreferencesManager(this);
        sharedPrefs.setLoggedIn(false);
        sharedPrefs.setBookmarks(null);
        sharedPrefs.setShirts(null);
        sharedPrefs.setPants(null);
        goToLogin();
    }

    private void goToLogin() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
