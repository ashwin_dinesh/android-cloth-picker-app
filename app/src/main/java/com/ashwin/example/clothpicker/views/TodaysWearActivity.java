package com.ashwin.example.clothpicker.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.ashwin.example.clothpicker.Constants;
import com.ashwin.example.clothpicker.ImageUtils;
import com.ashwin.example.clothpicker.R;
import com.ashwin.example.clothpicker.SharedPreferencesManager;
import com.ashwin.example.clothpicker.models.Cloth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

public class TodaysWearActivity extends AppCompatActivity {

    private static final String TAG = TodaysWearActivity.class.getSimpleName();
    private SharedPreferencesManager mSharedPrefs;
    private ArrayList<Cloth> mShirtsList, mPantsList;
    private ArrayList<String> mBookmarks;
    private String mTodaysWear = "";

    private AsyncTask<Void, Void, Bitmap> mAsyncTask;
    private ProgressDialog mProgressDialog;
    private ImageView mTodaysWearImageView;
    private Button mDislikeButton, mBookmarkButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_wear);

        initData();
        initViews();
        loadTodaysWear();
    }

    private void initData() {
        mSharedPrefs = new SharedPreferencesManager(this);
        mShirtsList = mSharedPrefs.getShirts();
        mPantsList = mSharedPrefs.getPants();
        mBookmarks = mSharedPrefs.getBookmarks();
    }

    private void initViews() {
        initActionBar();
        initProgressDialog();
        initImageView();
        initDislikeButton();
        initBookmarkButton();
    }

    private void initActionBar() {
        getSupportActionBar().setTitle("Todays Pick");
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
    }

    private void initImageView() {
        mTodaysWearImageView = (ImageView) findViewById(R.id.todaysWearImageView);
        mTodaysWearImageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initDislikeButton() {
        mDislikeButton = (Button) findViewById(R.id.dislikeButton);
        mDislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadTodaysWear();
            }
        });
    }

    private void initBookmarkButton() {
        mBookmarkButton = (Button) findViewById(R.id.bookmarkButton);
        mBookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveBookmark();
            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public int getRandomNumber(int start, int range) {
        Random rand = new Random();
        int randomNum = rand.nextInt(range) + start;
        return randomNum;
    }

    private void loadTodaysWear() {
        //showProgressDialog("Loading...");
        /*try {
            int shirtsListPosition = getRandomNumber(0, mShirtsList.size());
            int pantsListPosition = getRandomNumber(0, mPantsList.size());
            mTodaysWear = shirtsListPosition + "_" + pantsListPosition;
            Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : " + mTodaysWear);
            Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : shirtslist size : " + mShirtsList.size());
            Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : pantslist size : " + mPantsList.size());
            int w = 0, h = 0;

            h = mShirtsList.get(shirtsListPosition).getBitmap().getHeight() + 50 + mPantsList.get(pantsListPosition).getBitmap().getHeight();

            if (mShirtsList.get(shirtsListPosition).getBitmap().getWidth() > mPantsList.get(pantsListPosition).getBitmap().getWidth()) {
                w = mShirtsList.get(shirtsListPosition).getBitmap().getWidth();
            } else {
                w = mPantsList.get(pantsListPosition).getBitmap().getWidth();
            }

            Bitmap mergedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_4444);

            //Canvas canvas = new Canvas(mergedBitmap);
            //canvas.drawBitmap(mShirtsList.get(shirtsListPosition).getBitmap(), 0f, 0f, null);
            //canvas.drawBitmap(mPantsList.get(pantsListPosition).getBitmap(), 0f, mShirtsList.get(shirtsListPosition).getBitmap().getHeight() + 50f, null);

            //Bitmap bitmap = ImageUtils.mergeBitmap(mShirtsList.get(shirtsListPosition).getBitmap(), mPantsList.get(pantsListPosition).getBitmap());
            //mTodaysWearImageView.setImageBitmap(mShirtsList.get(shirtsListPosition).getBitmap());

            //Bitmap bitmap1 = BitmapFactory.decodeResource(this.getResources(), R.drawable.image1);
            Bitmap bitmap1 = mShirtsList.get(shirtsListPosition).getBitmap();
            //Bitmap bitmap2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.image2);
            Bitmap bitmap2 = mPantsList.get(pantsListPosition).getBitmap();
            mTodaysWearImageView.setImageBitmap(ImageUtils.mergeBitmap(this, bitmap1, bitmap2));

            //hideProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        mAsyncTask = new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog("Loading...");
            }

            String todaysWear = "";

            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap bitmap = null;
                try {
                    int shirtsListPosition = getRandomNumber(0, mShirtsList.size());
                    int pantsListPosition = getRandomNumber(0, mPantsList.size());
                    todaysWear = "shirt" + shirtsListPosition + "_" + "pant" + pantsListPosition;
                    Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : " + todaysWear);

                    //bitmap = ImageUtils.mergeBitmap(shirtsListPosition, pantsListPosition);
                    String base641 = mShirtsList.get(shirtsListPosition).getBase64();
                    String base642 = mPantsList.get(pantsListPosition).getBase64();
                    Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : doInBackground() : base64 1 : " + base641);
                    bitmap = ImageUtils.mergeBase64(base641, base642);
                    Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : doInBackground() : base64 2 : " + base642);

                    //byte[] decodedString = Base64.decode(base641, Base64.DEFAULT);
                    //bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                    return bitmap;
                } catch (Exception e) {
                    Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : doInBackground() : exception : " + e.toString());
                    e.printStackTrace();
                }

                Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : return null bitmap");
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (!isFinishing()) {
                    super.onPostExecute(bitmap);
                    Log.w(Constants.DEBUG_LOGGING, TAG + " : loadTodaysWear() : onPostExecute()");
                    hideProgressDialog();
                    mTodaysWearImageView.setImageBitmap(bitmap);
                    mTodaysWear = todaysWear;
                }
            }
        };

        mAsyncTask.execute();
    }

    private void saveBookmark() {
        mBookmarks.add(mTodaysWear);
        Log.w(Constants.DEBUG_LOGGING, TAG + " : saveBookmark() : bookmarks : " + mBookmarks.toString());
        mSharedPrefs.setBookmarks(mBookmarks);
        Toast.makeText(this, "Successfully bookmarked", Toast.LENGTH_LONG).show();
        goToBookmarks();
    }

    private void goToBookmarks() {
        Intent intent = new Intent(TodaysWearActivity.this, BookmarksActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onBackPressed()");
        if (isFinishing()) {
            onDestroy();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onPause()");
        super.onPause();
    }

    private void unbindDrawables(View view) {
        //Runtime.getRuntime().gc();
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    @Override
    protected void onDestroy() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onDestroy()");
        mTodaysWearImageView = null;
        mShirtsList = null;
        mPantsList = null;
        mBookmarks = null;
        super.onDestroy();
    }
}
