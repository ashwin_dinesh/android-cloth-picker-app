package com.ashwin.example.clothpicker;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.ashwin.example.clothpicker.models.Cloth;
import com.ashwin.example.clothpicker.models.Clothes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Ashwin on 22-09-2017.
 */

public class SharedPreferencesManager {

    private static final String TAG = SharedPreferencesManager.class.getSimpleName();
    private Context mContext;
    private static SharedPreferences mSharedPreferences;
    private static final String PREFERENCES = "my_preferences";


    public SharedPreferencesManager(Context context)
    {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        mContext = context;
    }


    /**
     * login flag
     */
    private static final String LOGIN_FLAG = "Login Flag";

    public boolean getLoggedIn()
    {
        return mSharedPreferences.getBoolean(LOGIN_FLAG, false);
    }

    public void setLoggedIn(boolean boolLogin)
    {
        mSharedPreferences.edit().putBoolean(LOGIN_FLAG, boolLogin).apply();
    }

    /**
     * Shirts
     */
    private static final String SHIRTS = "Shirts";

    public ArrayList<Cloth> getShirts() {
        List<Cloth> clothesList = null;
        String clothes_string = mSharedPreferences.getString(SHIRTS, "");
        if (clothes_string.equals("")) {
            clothesList = null;
        } else {
            Clothes clothes;
            try {
                //clothes = LoganSquare.parse(clothes_string, Clothes.class);
                Gson gson = new Gson();
                clothes = gson.fromJson(clothes_string, Clothes.class);
                clothesList = clothes.getClothes();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ArrayList<Cloth> clothesArrayList = new ArrayList<>();
        if (clothesList != null) {
            clothesArrayList.addAll(clothesList);
        }
        return clothesArrayList;
    }

    public void setShirts( ArrayList<Cloth> clothesList ) {
        String jsonString = "";
        if (clothesList == null || clothesList.size() == 0) {
            // Do nothing
        } else {
            Clothes clothes = new Clothes();
            clothes.setClothes(clothesList);
            try {
                Gson gson = new Gson();
                jsonString = gson.toJson(clothes);
                //jsonString = LoganSquare.serialize(clothes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mSharedPreferences.edit().putString(SHIRTS, jsonString).apply();
    }

    /**
     * Pants
     */
    private static final String PANTS = "Pants";

    public ArrayList<Cloth> getPants() {
        List<Cloth> clothesList = null;
        String clothes_string = mSharedPreferences.getString(PANTS, "");
        if (clothes_string.equals("")) {
            clothesList = null;
        } else {
            Clothes clothes;
            try {
                //clothes = LoganSquare.parse(clothes_string, Clothes.class);
                Gson gson = new Gson();
                clothes = gson.fromJson(clothes_string, Clothes.class);
                clothesList = clothes.getClothes();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ArrayList<Cloth> clothesArrayList = new ArrayList<>();
        if (clothesList != null) {
            clothesArrayList.addAll(clothesList);
        }
        return clothesArrayList;
    }

    public void setPants( ArrayList<Cloth> clothesList ) {
        String jsonString = "";
        if (clothesList == null || clothesList.size() == 0) {
            // Do nothing
        } else {
            Clothes clothes = new Clothes();
            clothes.setClothes(clothesList);
            try {
                Gson gson = new Gson();
                jsonString = gson.toJson(clothes);
                //jsonString = LoganSquare.serialize(clothes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mSharedPreferences.edit().putString(PANTS, jsonString).apply();
    }

    /**
     * Bookmarks
     */
    private static final String BOOKMARKS = "Bookmarks";

    public ArrayList<String> getBookmarks() {
        Set<String> bookmarks = new HashSet<>();
        bookmarks = mSharedPreferences.getStringSet(BOOKMARKS, bookmarks);
        return new ArrayList<String>(bookmarks);
    }

    public void setBookmarks( ArrayList<String> bookmarks ) {
        Set<String> set = new HashSet<>();
        if (bookmarks != null) {
            Log.w(Constants.DEBUG_LOGGING, TAG + " : setBookmarks() : bookmarks : " + bookmarks.toString());
            for (String s : bookmarks) {
                set.add(s);
            }
        }
        mSharedPreferences.edit().putStringSet(BOOKMARKS, set).apply();
    }
}
