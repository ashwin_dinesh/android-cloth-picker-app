package com.ashwin.example.clothpicker.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.Toast;

import com.ashwin.example.clothpicker.Constants;
import com.ashwin.example.clothpicker.ImageUtils;
import com.ashwin.example.clothpicker.R;
import com.ashwin.example.clothpicker.SharedPreferencesManager;
import com.ashwin.example.clothpicker.models.Cloth;

import java.io.IOException;
import java.util.ArrayList;

public class AddClothesActivity extends AppCompatActivity implements RecyclerAdapter.AddImageListener {

    private static final String TAG = AddClothesActivity.class.getSimpleName();
    private String TYPE = Constants.SHIRTS;
    private static final int STORAGE_PERMISSION_REQUEST = 100;
    private static final int IMAGE_REQUEST = 101;
    private static final int CAMERA_PERMISSION_REQUEST = 102;
    private static final int CAMERA_REQUEST = 103;
    private Bitmap mImageBitmap;
    private ArrayList<Cloth> mClothesArray;
    private ArrayList<String> mImageNamesArray;
    private static final int mMaxWidth = 200, mMaxHeight = 200, mQuality = 100;
    private int mNextImageNumber = 1;
    private SharedPreferencesManager mSharedPrefs;

    private RecyclerAdapter mRecyclerAdapter;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mGridLayoutManager;
    private Button mDoneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_clothes);

        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            TYPE = bundle.getString(Constants.TYPE, Constants.SHIRTS);
        }

        initData();
        initViews();
    }

    private void initData() {
        mSharedPrefs = new SharedPreferencesManager(this);
    }

    private void initViews() {
        initActionBar();
        initRecyclerView();
        initDoneButton();
    }

    private void initActionBar() {
        if (TYPE.equals(Constants.SHIRTS)) {
            getSupportActionBar().setTitle("Add Shirts");
        } else {
            getSupportActionBar().setTitle("Add Pants");
        }
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mGridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        mClothesArray = new ArrayList<>();
        mImageNamesArray = new ArrayList<>();
        mRecyclerAdapter = new RecyclerAdapter(this, this, mClothesArray);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    private void initDoneButton() {
        mDoneButton = (Button) findViewById(R.id.doneButton);
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (saveClothes()) {
                    if (TYPE.equals(Constants.SHIRTS)) {
                        goToClothesActivity();
                    } else {
                        goToTodaysWear();
                    }
                } else {
                    Toast.makeText(AddClothesActivity.this, "Please upload images", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean saveClothes() {
        if (mClothesArray == null || mClothesArray.size() == 0) {
            return false;
        }
        if (TYPE.equals(Constants.SHIRTS)) {
            int i = 0;
            for (Cloth c : mClothesArray) {
                Bitmap b = c.getBitmap();
                ImageUtils.saveImageBitmap(b, "shirt" + i);
                mClothesArray.get(i).setBase64(ImageUtils.getBase64StringImage(b));
                mClothesArray.get(i).setBitmap(null);
                i++;
            }
            mSharedPrefs.setShirts(mClothesArray);
        } else {
            int i = 0;
            for (Cloth c : mClothesArray) {
                Bitmap b = c.getBitmap();
                ImageUtils.saveImageBitmap(b, "pant" + i);
                mClothesArray.get(i).setBase64(ImageUtils.getBase64StringImage(b));
                mClothesArray.get(i).setBitmap(null);
                i++;
            }
            mSharedPrefs.setPants(mClothesArray);
        }
        return true;
    }

    private void goToClothesActivity() {
        Intent intent = new Intent(AddClothesActivity.this, AddClothesActivity.class);
        intent.putExtra(Constants.TYPE, Constants.PANTS);
        startActivity(intent);
        finish();
    }

    private void goToTodaysWear() {
        Intent intent = new Intent(AddClothesActivity.this, TodaysWearActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCameraClicked(int position) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
        } else {
            openCamera();
        }
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    public void onGalleryClicked(int position) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
        } else {
            showFileChooser();
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_REQUEST) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                // Permission granted
                showFileChooser();
            } else {
                // Permission denied
                Toast.makeText(this, "Need Storage Permission to upload image", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == CAMERA_PERMISSION_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted
                openCamera();
            } else {
                // Permission denied
                Toast.makeText(this, "Need Camera Permission to capture image", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                mImageBitmap = ImageUtils.getCompressedBitmap(mImageBitmap, mQuality);
                mImageBitmap = ImageUtils.getScaledBitmap(mImageBitmap, mMaxWidth, mMaxHeight);
                updateRecyclerAdapter(mImageBitmap);
            } catch (IOException e) {
                Log.d(Constants.DEBUG_LOGGING, TAG + " : onActivityResult() : IOException : " + e.toString());
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                try {
                    mImageBitmap = (Bitmap) data.getExtras().get("data");
                    mImageBitmap = ImageUtils.getCompressedBitmap(mImageBitmap, mQuality);
                    mImageBitmap = ImageUtils.getScaledBitmap(mImageBitmap, mMaxWidth, mMaxHeight);
                    updateRecyclerAdapter(mImageBitmap);
                } catch (Exception e) {
                    Log.d(Constants.DEBUG_LOGGING, TAG + " : OnActivityResult() : CAMERA REQUEST : Exception : " + e.toString());
                    e.printStackTrace();
                    Toast.makeText(this, "Oops! Try using default camera app", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void updateRecyclerAdapter(Bitmap bitmap) {
        String filename = "";
        if (TYPE.equals(Constants.SHIRTS)) {
            filename = "shirt_" + mNextImageNumber;
        } else {
            filename = "pant_" + mNextImageNumber;
        }
        mClothesArray.add(new Cloth(bitmap, filename));
        mRecyclerAdapter.notifyDataSetChanged();
        mNextImageNumber++;
        //ImageUtils.saveImageBitmap(this, bitmap, filename);
        //mImageNamesArray.add(filename);
        //Log.w(Constants.DEBUG_LOGGING, "clothes : " + mImageNamesArray.toString());
        Log.w(Constants.DEBUG_LOGGING, "clothes : " + mClothesArray.toString());
    }

    @Override
    public void onRemoveClicked(int position) {
        mClothesArray.remove(position);
        mRecyclerAdapter.notifyDataSetChanged();
        //String filename = mClothesArray.get(position).getFilename();
        //ImageUtils.deleteImageBitmap(this, filename);
        Log.w(Constants.DEBUG_LOGGING, "clothes : " + mClothesArray.toString());
    }

    @Override
    protected void onDestroy() {
        mClothesArray = null;
        mRecyclerAdapter = null;
        super.onDestroy();
    }
}
