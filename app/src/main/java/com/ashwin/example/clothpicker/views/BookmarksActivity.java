package com.ashwin.example.clothpicker.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ashwin.example.clothpicker.Constants;
import com.ashwin.example.clothpicker.ImageUtils;
import com.ashwin.example.clothpicker.R;
import com.ashwin.example.clothpicker.SharedPreferencesManager;
import com.ashwin.example.clothpicker.models.Cloth;

import java.util.ArrayList;
import java.util.Set;

public class BookmarksActivity extends AppCompatActivity implements BookmarksRecyclerAdapter.BookmarkActionListener {

    private static final String TAG = BookmarksActivity.class.getSimpleName();
    private SharedPreferencesManager mSharedPrefs;
    private ArrayList<String> mBookmarks;
    private ArrayList<Bitmap> mBookmarkBitmaps;

    private TextView mNoContentTextView;
    private RecyclerView mBookmarksRecyclerView;
    private BookmarksRecyclerAdapter mBookmarkRecyclerAdapter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmarks);

        initData();
        initViews();
        loadBookmarks();
    }

    private void initData() {
        mSharedPrefs = new SharedPreferencesManager(this);
        mBookmarks = mSharedPrefs.getBookmarks();
        Log.w(Constants.DEBUG_LOGGING, TAG + " : initData() : bookmarks : " + mBookmarks.toString());
    }

    private void initViews() {
        initActionBar();
        initNoContentText();
        initProgressDialog();
        initRecyclerView();
    }

    private void initActionBar() {
        getSupportActionBar().setTitle("Bookmarks");
    }

    private void initNoContentText() {
        mNoContentTextView = (TextView) findViewById(R.id.noContentTextView);
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
    }

    private void initRecyclerView() {
        mBookmarksRecyclerView = (RecyclerView) findViewById(R.id.bookmarksRecyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mBookmarksRecyclerView.setLayoutManager(gridLayoutManager);

        mBookmarkBitmaps = new ArrayList<>();
        mBookmarkRecyclerAdapter = new BookmarksRecyclerAdapter(this, this, mBookmarkBitmaps);
        mBookmarksRecyclerView.setAdapter(mBookmarkRecyclerAdapter);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private void loadBookmarks() {
        new AsyncTask<Void, Void, ArrayList<Bitmap>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showProgressDialog("Loading...");
            }

            @Override
            protected ArrayList<Bitmap> doInBackground(Void... params) {
                try {
                    for (String bookmark : mBookmarks) {
                        String[] array = bookmark.split("_");
                        Bitmap bitmap = ImageUtils.mergeBitmap(array[0], array[1]);
                        mBookmarkBitmaps.add(bitmap);
                    }
                    return mBookmarkBitmaps;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(ArrayList<Bitmap> bookmarkBitmaps) {
                super.onPostExecute(bookmarkBitmaps);
                hideProgressDialog();
                mBookmarkRecyclerAdapter.notifyDataSetChanged();
                isNoContent();
            }
        }.execute();
    }

    @Override
    public void onShareClicked(int position) {
        ImageUtils.shareImageBitmap(this, mBookmarkBitmaps.get(position));
    }

    @Override
    public void onRemoveClicked(int position) {
        mBookmarkBitmaps.remove(position);
        mBookmarks.remove(position);
        mSharedPrefs.setBookmarks(mBookmarks);
        mBookmarkRecyclerAdapter.notifyDataSetChanged();
        isNoContent();
    }

    private void isNoContent() {
        if (mBookmarkBitmaps.size() == 0) {
            mNoContentTextView.setVisibility(View.VISIBLE);
        } else {
            mNoContentTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onBackPressed()");
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onPause()");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.w(Constants.DEBUG_LOGGING, TAG + " : onDestroy()");
        mBookmarkBitmaps = null;
        mBookmarkRecyclerAdapter = null;
        mBookmarksRecyclerView = null;
        mBookmarks = null;
        mSharedPrefs = null;
        super.onDestroy();
    }
}
