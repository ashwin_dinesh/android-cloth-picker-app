package com.ashwin.example.clothpicker.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ashwin on 23/9/17.
 */

public class Cloth implements Parcelable {

    public Cloth() { }

    public Cloth(Bitmap bitmap, String filename) {
        this.bitmap = bitmap;
        this.filename = filename;
    }

    private String filename;
    private Bitmap bitmap;
    private String base64;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    @Override
    public String toString() {
        return filename;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.filename);
        dest.writeParcelable(this.bitmap, flags);
        dest.writeString(this.base64);
    }

    protected Cloth(Parcel in) {
        this.filename = in.readString();
        this.bitmap = in.readParcelable(Bitmap.class.getClassLoader());
        this.base64 = in.readString();
    }

    public static final Creator<Cloth> CREATOR = new Creator<Cloth>() {
        @Override
        public Cloth createFromParcel(Parcel source) {
            return new Cloth(source);
        }

        @Override
        public Cloth[] newArray(int size) {
            return new Cloth[size];
        }
    };
}
