package com.ashwin.example.clothpicker;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Ashwin on 22-09-2017.
 */

public class ImageUtils {

    private static final String TAG = ImageUtils.class.getSimpleName();

    public static long getImageSize(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return getByteArraySize(baos);
    }

    public static long getByteArraySize(ByteArrayOutputStream baos) {
        byte[] imageInByte = baos.toByteArray();
        long lengthbmp = imageInByte.length;
        return lengthbmp;
    }

    public static Bitmap getCompressedBitmap(Bitmap bitmap, int quality) {
        if (quality <= 0 || quality > 100) {
            quality = 100;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
        try {
            baos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap, int requiredWidth, int requiredHeight) {
        int originalWidth = bitmap.getWidth();
        int originalHeight = bitmap.getHeight();
        Log.w(Constants.DEBUG_LOGGING, TAG + " : original image : width : " + originalWidth + ", height : " + originalHeight);

        /*if (originalWidth > originalHeight) {
            int temp = originalWidth;
            originalWidth = originalHeight;
            originalHeight = temp;
        }*/

        int resizedWidth = originalWidth;
        int resizedHeight = originalHeight;

        if (resizedWidth > requiredWidth) {
            int ratio = originalWidth / requiredWidth;
            if (ratio > 0) {
                resizedWidth = originalWidth / ratio;
                resizedHeight = originalHeight / ratio;
            }
        }
        Log.w(Constants.DEBUG_LOGGING, TAG + " : scaled image pass 1 : width : " + resizedWidth + ", height : " + resizedHeight);

        if (resizedHeight > requiredHeight) {
            int ratio = originalHeight / requiredHeight;
            if (ratio > 0) {
                resizedHeight = originalHeight / ratio;
                resizedWidth = originalWidth / ratio;
            }
        }

        Log.w(Constants.DEBUG_LOGGING, TAG + " : scaled image pass 2 : width : " + resizedWidth + ", height : " + resizedHeight);
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, true);
    }

    public static String getBase64StringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        return encodedImage;
    }

    public static Bitmap mergeBitmap(Bitmap bitmap1, Bitmap bitmap2) {
        Context context = AppController.getAppContext();
        saveImageBitmap(bitmap1, "bitmap1.jpg");
        saveImageBitmap(bitmap2, "bitmap2.jpg");

        Bitmap mergedBitmap = null;

        int w = 0, h = 0;

        h = bitmap1.getHeight() + 50 + bitmap2.getHeight();

        if (bitmap1.getWidth() > bitmap2.getWidth()) {
            w = bitmap1.getWidth();
        } else {
            w = bitmap2.getWidth();
        }

        mergedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(mergedBitmap);
        canvas.drawBitmap(loadImage(context, "bitmap1.jpg"), 0f, 0f, null);
        canvas.drawBitmap(loadImage(context, "bitmap2.jpg"), 0f, bitmap1.getHeight() + 50f, null);

        return mergedBitmap;
    }

    public static Bitmap mergeBitmap(int shirtPosition, int pantPosition) {
        Context context = AppController.getAppContext();
        Bitmap bitmap1 = loadImage(context, "shirt" + shirtPosition);
        Bitmap bitmap2 = loadImage(context, "pant" + pantPosition);

        Bitmap mergedBitmap = null;

        int w = 0, h = 0;

        h = bitmap1.getHeight() + 25 + bitmap2.getHeight();

        if (bitmap1.getWidth() > bitmap2.getWidth()) {
            w = bitmap1.getWidth();
        } else {
            w = bitmap2.getWidth();
        }

        mergedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(mergedBitmap);
        canvas.drawBitmap(loadImage(context, "shirt" + shirtPosition), 0f, 0f, null);
        canvas.drawBitmap(loadImage(context, "pant" + pantPosition), 0f, bitmap1.getHeight() + 25f, null);

        return mergedBitmap;
    }

    public static Bitmap mergeBitmap(String shirt, String pant) {
        Bitmap bitmap1 = loadImage(AppController.getAppContext(), shirt);
        Bitmap bitmap2 = loadImage(AppController.getAppContext(), pant);

        Bitmap mergedBitmap = null;

        int w = 0, h = 0;

        h = bitmap1.getHeight() + 25 + bitmap2.getHeight();

        if (bitmap1.getWidth() > bitmap2.getWidth()) {
            w = bitmap1.getWidth();
        } else {
            w = bitmap2.getWidth();
        }

        mergedBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        final Canvas canvas = new Canvas(mergedBitmap);
        canvas.drawBitmap(bitmap1, 0f, 0f, null);
        canvas.drawBitmap(bitmap2, 0f, bitmap1.getHeight() + 25f, null);

        return mergedBitmap;
    }

    public static Bitmap mergeBase64(String base641, String base642) {
        byte[] decodedString = Base64.decode(base641, Base64.DEFAULT);
        Bitmap bitmap1 = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        decodedString = Base64.decode(base642, Base64.DEFAULT);
        Bitmap bitmap2 = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return mergeBitmap(bitmap1, bitmap2);
    }

    public static void saveImageBitmap(Bitmap bitmap, String name){
        Context context = AppController.getAppContext();
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteImageBitmap(Context context, String name) {
        context.deleteFile(name);
    }

    /**
     * Save image
     */
    public static void saveImage(Context context, Bitmap bitmap, String name, String extension) {
        name = name + "." + extension;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Load image
     */
    public static Bitmap loadImage(Context context, String name){
        FileInputStream fileInputStream = null;
        Bitmap bitmap = null;
        try {
            fileInputStream = context.openFileInput(name);
            bitmap = BitmapFactory.decodeStream(fileInputStream);
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    public static void shareImage(Context context, String filename) {
        try {
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), filename, "Title", null);
            Uri imageUri = Uri.parse(path);

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);
            intent.putExtra(Intent.EXTRA_TEXT, "");
            context.startActivity(Intent.createChooser(intent, "Share with"));
        } catch (Exception e) {
            Log.e("Error on sharing", e + " ");
            Toast.makeText(context, "App not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    public static void shareImageBitmap(Context context, Bitmap bitmap) {
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
            Uri imageUri = Uri.parse(path);

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);
            intent.putExtra(Intent.EXTRA_TEXT, "");
            context.startActivity(Intent.createChooser(intent, "Share with"));
        } catch (Exception e) {
            Log.e("Error on sharing", e + " ");
            Toast.makeText(context, "App not Installed", Toast.LENGTH_SHORT).show();
        }
    }
}
