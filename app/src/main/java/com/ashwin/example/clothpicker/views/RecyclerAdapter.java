package com.ashwin.example.clothpicker.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ashwin.example.clothpicker.R;
import com.ashwin.example.clothpicker.models.Cloth;

import java.util.ArrayList;

/**
 * Created by ashwin on 23/9/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface AddImageListener {
        void onCameraClicked(int position);
        void onGalleryClicked(int position);
        void onRemoveClicked(int position);
    }

    private static final int ADD_VIEW = 1, IMAGE_VIEW = 2;
    private Context mContext;
    private AddImageListener mAddImageListener;
    private ArrayList<Cloth> mClothes;

    public RecyclerAdapter(Context context, AddImageListener addImageListener, ArrayList<Cloth> cloths) {
        mContext = context;
        mAddImageListener = addImageListener;
        mClothes = cloths;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return (position >= mClothes.size()) ? ADD_VIEW : IMAGE_VIEW;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case ADD_VIEW:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_add_cloth, parent, false);
                holder = new AddImageViewHolder(view);
                break;
            case IMAGE_VIEW:
                view = LayoutInflater.from(mContext).inflate(R.layout.layout_image, parent, false);
                holder = new ImageViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AddImageViewHolder) {
            final AddImageViewHolder addImageViewHolder = (AddImageViewHolder) holder;
            (addImageViewHolder).position = position;
        } else if (holder instanceof ImageViewHolder) {
            final ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
            Cloth cloth = mClothes.get(position);
            (imageViewHolder).position = position;
            (imageViewHolder).mImageView.setImageBitmap(cloth.getBitmap());
        }
    }

    @Override
    public int getItemCount() {
        return mClothes.size() + 1;
    }

    private class AddImageViewHolder extends RecyclerView.ViewHolder {
        int position;
        ImageView mCameraImageButton, mGalleryImageButton;

        public AddImageViewHolder(View itemView) {
            super(itemView);

            initViews(itemView);
        }

        private void initViews(View itemView) {
            mCameraImageButton = (ImageView) itemView.findViewById(R.id.cameraImageButton);
            mCameraImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddImageListener != null) {
                        mAddImageListener.onCameraClicked(position);
                    }
                }
            });

            mGalleryImageButton = (ImageView) itemView.findViewById(R.id.galleryImageButton);
            mGalleryImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddImageListener != null) {
                        mAddImageListener.onGalleryClicked(position);
                    }
                }
            });
        }
    }

    private class ImageViewHolder extends RecyclerView.ViewHolder {
        int position;
        ImageView mImageView, mRemoveImageView;
        public ImageViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView);

            mRemoveImageView = (ImageView) itemView.findViewById(R.id.removeImageView);
            mRemoveImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAddImageListener != null) {
                        mAddImageListener.onRemoveClicked(position);
                    }
                }
            });
        }
    }
}
